import discord
from discord.ext import commands, tasks
import config
import json
import pokedex
from datetime import datetime, timedelta

def generate_Raid(raid,gym,gym_details):
    Ergebnis = []

    #gym_details
    gym_id = gym_details[0]
    name = gym_details[1]
    description = gym_details[2]
    url = gym_details[3]
    last_scanned= gym_details[4]+ timedelta(hours=2)

    #gym
    gym_id = gym[0]
    team_id = gym[1]
    guard_pokemon_id = gym[2]
    slots_avaible = gym[3]
    enabled = gym[4]
    latitude = gym[5]
    longitude = gym[6]
    total_cp = gym[7]
    is_in_battle = gym[8]
    gender = gym[9]
    form = gym[10]
    costume = gym[11]
    weather_boosted_condition = gym[12]
    shiny = gym[13]
    last_modified = gym[14] + timedelta(hours=2)
    last_scanned = gym[15]+ timedelta(hours=2)
    is_ex_raid_eligible = gym[16]
    is_ar_scan_eligible = gym[17]

    #raid
    gym_id = raid[0]
    level= raid[1]
    spawn= raid[2]+ timedelta(hours=2)
    start = raid[3]+ timedelta(hours=2)
    end = raid[4]+ timedelta(hours=2)
    pokemon_id = raid[5]
    cp = raid[6]
    move1 = raid[7]
    move2 = raid[8]
    form= raid[9] 
    is_exclusive = raid[10]
    gender= raid[11]
    costume = raid[12]
    evolution= raid[13]

    #print(pokemon)

    Link_Google = "http://maps.google.com/maps?q="+str(latitude)+","+str(longitude)
    Link_Apple = "http://maps.apple.com/maps?daddr="+str(latitude)+","+str(longitude)+"&z=10&t=s&dirflg=w"

    if end > datetime.now():
        if pokemon_id != None:
            #pokemon
            pokemon = pokedex.give_data_for_everything(str(pokemon_id),6)
            pokemon_species_id = pokemon[0]
            local_language_id= pokemon[1]
            pokemon_species_name= pokemon[2]
            pokemon_species_genus= pokemon[3]
            pokemon_species_types= pokemon[4]
            pokemon_species_bild= pokemon[5]

            if latitude < config.latitude1 and latitude > config.latitude2:
                if longitude < config.longitude1 and longitude >config.longitude2:

                    #Embed
                    embed=discord.Embed(title=name, description="Ein Raid ist da", color=0x0080ff)
                    embed.set_author(name=pokemon_species_name, url=pokemon_species_bild, icon_url=pokemon_species_bild)
                    embed.set_thumbnail(url=url)
                    embed.add_field(name="Level", value=str(level), inline=False)
                    embed.add_field(name="Spawn", value=str(spawn), inline=False)
                    embed.add_field(name="Start", value=str(start), inline=False)
                    embed.add_field(name="Ende", value=str(end), inline=False)
                    embed.add_field(name="Maplink", value="[GoogleMap]("+Link_Google+")", inline=False)
                    embed.add_field(name="Maplink", value="[AppleMap]("+Link_Apple+")", inline=False)
                    Ergebnis.append(embed)
        else:
            if latitude < config.latitude1 and latitude > config.latitude2:
                if longitude < config.longitude1 and longitude >config.longitude2:
                    #Embed
                    if level == 1:
                        pass
                        pokemon_species_bild = "https://static.pokebattler.com/images/solo_raid_egg.png"
                    if level == 3:
                        pokemon_species_bild = "https://static.pokebattler.com/images/group_raid_egg.png"
                    if level == 5: 
                        pokemon_species_bild = "https://static.pokebattler.com/images/legendary_raid_egg.png"
                    if level == 6:
                        pokemon_species_bild = "https://static.pokebattler.com/images/mega_raid_egg.png"
                    embed=discord.Embed(title=name, description="Ein Raid ist da", color=0x0080ff)
                    embed.set_author(name="Level "+str(level), url=pokemon_species_bild, icon_url=pokemon_species_bild)
                    embed.set_thumbnail(url=url)
                    embed.add_field(name="Spawn", value=str(spawn), inline=False)
                    embed.add_field(name="Start", value=str(start), inline=False)
                    embed.add_field(name="Ende", value=str(end), inline=False)
                    embed.add_field(name="Maplink", value="[GoogleMap]("+Link_Google+")", inline=False)
                    embed.add_field(name="Maplink", value="[AppleMap]("+Link_Apple+")", inline=False)
                    Ergebnis.append(embed)

    return(Ergebnis)

def generate_Pokemon(pokemon):
    Ergebnis = []
    encounter_id = pokemon[0]
    pokemon_id= pokemon[1]
    latitude= pokemon[2]
    longitude= pokemon[3]
    disappear_time= pokemon[4]+ timedelta(hours=2)
    individual_attack= pokemon[5]
    individual_defense= pokemon[6]
    individual_stamina= pokemon[7]
    cp= pokemon[8]

    pokemon = pokedex.give_data_for_everything(str(pokemon_id),6)
    pokemon_species_id = pokemon[0]
    local_language_id= pokemon[1]
    pokemon_species_name= pokemon[2]
    pokemon_species_genus= pokemon[3]
    pokemon_species_types= pokemon[4]
    pokemon_species_bild= pokemon[5]
    Link_Google = "http://maps.google.com/maps?q="+str(latitude)+","+str(longitude)
    Link_Apple = "http://maps.apple.com/maps?daddr="+str(latitude)+","+str(longitude)+"&z=10&t=s&dirflg=w"

    if individual_stamina != None:
        if individual_attack != None:
            if individual_defense != None:
                embed=discord.Embed(title=pokemon_species_name, description="Ein wildes "+str(pokemon_species_name)+" ist aufgetaucht", color=0x0080ff)
                embed.set_thumbnail(url=pokemon_species_bild)
                embed.add_field(name="verfügbar bis", value=str(disappear_time), inline=False)
                embed.add_field(name="CP", value=str(cp), inline=False)
                embed.add_field(name="Attack", value=str(individual_attack), inline=False)
                embed.add_field(name="Defense", value=str(individual_defense), inline=False)
                embed.add_field(name="Stamina", value=str(individual_stamina), inline=False)
                embed.add_field(name="Maplink", value="[GoogleMap]("+Link_Google+")", inline=False)
                embed.add_field(name="Maplink", value="[AppleMap]("+Link_Apple+")", inline=False)
                Ergebnis.append(embed)
    else:
        embed=discord.Embed(title=pokemon_species_name, description="Ein wildes "+str(pokemon_species_name)+" ist aufgetaucht", color=0x0080ff)
        embed.set_thumbnail(url=pokemon_species_bild)
        embed.add_field(name="verfügbar bis", value=str(disappear_time), inline=False)
    
        embed.add_field(name="Maplink", value="[GoogleMap]("+Link_Google+")", inline=False)
        embed.add_field(name="Maplink", value="[AppleMao]("+Link_Apple+")", inline=False)
        Ergebnis.append(embed)
    return(Ergebnis)


def generate_pokemon_for_user(pokemon_id):
    pokemon = pokedex.give_data_for_everything(str(pokemon_id),6)
    pokemon_species_id = pokemon[0]
    local_language_id= pokemon[1]
    pokemon_species_name= pokemon[2]
    pokemon_species_genus= pokemon[3]
    pokemon_species_types= pokemon[4]
    pokemon_species_bild= pokemon[5]

    embed=discord.Embed(title=pokemon_species_name, description="Erfolgreich hinzugefügt", color=0x0080ff)
    embed.set_thumbnail(url=pokemon_species_bild)
    embed.add_field(name="Erfolg", value="Ich melde dir nun "+str(pokemon_species_name), inline=False)
    return(embed)
                
def generate_pokemon_for_user_del(pokemon_id):
    pokemon = pokedex.give_data_for_everything(str(pokemon_id),6)
    pokemon_species_id = pokemon[0]
    local_language_id= pokemon[1]
    pokemon_species_name= pokemon[2]
    pokemon_species_genus= pokemon[3]
    pokemon_species_types= pokemon[4]
    pokemon_species_bild= pokemon[5]

    embed=discord.Embed(title=pokemon_species_name, description="Erfolgreich entfernt", color=0x0080ff)
    embed.set_thumbnail(url=pokemon_species_bild)
    embed.add_field(name="Erfolg", value="Ich melde dir nun "+str(pokemon_species_name)+" nicht mehr", inline=False)
    return(embed)
                