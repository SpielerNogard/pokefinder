import discord
from discord.ext import commands, tasks


def HilfePoki():
    embed=discord.Embed(title="Help", description="Meine Hilfeseite", color=0x0080ff)
    embed.set_thumbnail(url="https://assets.pokemon.com/assets/cms2/img/pokedex/full/113.png")
    embed.add_field(name="!help", value="Zeigt diese Seite an", inline=False)
    embed.add_field(name="!add {arg1}", value="Fügt ein Pokemon zu deiner Wunschliste hinzu z.B: !add Rayquaza", inline=False)
    embed.add_field(name="!ivadd {arg1}", value="Fügt das Pokemon zu deiner Liste hinzu allerdings nur IV > 90 z.B !ivadd onix", inline=False)
    embed.add_field(name="!list ", value="Zeigt dir deine Wunschliste an", inline=False)
    embed.add_field(name="!del {arg1}", value="Löscht das Pokemon von deiner Liste", inline=False)
    
    return(embed)