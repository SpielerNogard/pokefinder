import discord
from discord.ext import commands,tasks
from discord.utils import get

import config
import embed_generator
import pokedex
import Hilfe

from Raid_Handler import RaidHandler
from Gym_Handler import Gym_Handler
from Pokemon_Manager import Pokemon_Manager
from UserIV_Handler import User_IV_Handler

bot = commands.Bot(command_prefix="!")
GUSTAV = RaidHandler()
HARRY = Gym_Handler()
AUGUST = Pokemon_Manager()
GLURAK = User_IV_Handler()
@bot.event
async def on_ready():
    print(f'{bot.user.name} has connected to Discord!')
    print("Changing Status .......")
    await bot.change_presence(status=discord.Status.idle, activity=discord.Game('Starting ..... '))
    await bot.change_presence(status=discord.Status.online, activity=discord.Game('!help'))
    if config.report_Raids == True:
        report_Raids.start()
    
    report_Pokemon.start()
    
@bot.event
async def on_message(Message):
    try:
        Befehl = Message.content
        Befehl = Befehl.lower()
        User = Message.author
        Channel = Message.channel

        if Befehl.startswith("!help"):
            embed = Hilfe.HilfePoki()
            await Channel.send(embed=embed)

        if Befehl.startswith("!add"):
            Suche = Befehl
            user_id = User.id
            Search = Suche.replace("!add ", "")
            pokemon_id = pokedex.give_data_for_everything(Search, 6)[0]
            #print(pokemon_id)
            vorhanden = GLURAK.is_user_in_non_iv(user_id,pokemon_id)
            if vorhanden == False:
                Erfolg = GLURAK.create_pokemon_without_iv(user_id,pokemon_id)
                if Erfolg == True:
                    embed = embed_generator.generate_pokemon_for_user(pokemon_id)
                    await User.send(embed=embed)
                else:
                    await User.send("Leider ist ein Fehler aufgetreten")
            else:
                User.send("Dieses Pokemon existiert schon in deiner Liste")

        if Befehl.startswith("!ivadd"):
            Suche = Befehl
            user_id = User.id
            Search = Suche.replace("!ivadd ", "")
            pokemon_id = pokedex.give_data_for_everything(Search, 6)[0]
            #print(pokemon_id)
            vorhanden = GLURAK.is_user_in_with_iv(user_id,pokemon_id)
            if vorhanden == False:
                Erfolg = GLURAK.create_pokemon_with_iv(user_id,pokemon_id)
                if Erfolg == True:
                    embed = embed_generator.generate_pokemon_for_user(pokemon_id)
                    await User.send(embed=embed)
                else:
                    await User.send("Leider ist ein Fehler aufgetreten")
            else:
                User.send("Dieses Pokemon existiert schon in deiner Liste")

        if Befehl.startswith("!list"):
            Suche = Befehl
            user_id = User.id
            falsches_Ergebnis = ["Ich konnte keine Pokemon-Liste finden","Ich konnte keine IV-Liste finden"]
            Ergebnis = GLURAK.find_my_list(user_id)
            if Ergebnis != falsches_Ergebnis:
                mit_iv = Ergebnis[1]
                ohne_iv = Ergebnis[0]

                neu_mit = []
                neu_ohne = []
                for a in mit_iv:
                    pokemon_name = pokedex.give_data_for_everything(str(a), 6)[2]
                    neu_mit.append(pokemon_name)
                for b in ohne_iv:
                    pokemon_name = pokedex.give_data_for_everything(str(b), 6)[2]
                    neu_ohne.append(pokemon_name)

                Ergebnis = [neu_ohne,neu_mit]
            await User.send("Hier ist deine Liste")
            await User.send("ohne IV")
            await User.send(str(Ergebnis[0]))
            await User.send("mit IV")
            await User.send(str(Ergebnis[1]))

        if Befehl.startswith("!del"):
            Suche = Befehl
            user_id = User.id
            Search = Suche.replace("!del ", "")
            pokemon_id = pokedex.give_data_for_everything(Search, 6)[0]
            #print(pokemon_id)
            Erfolg = GLURAK.delete_pokemon_from_list(user_id,pokemon_id)
            
            if Erfolg == True:
                embed = embed_generator.generate_pokemon_for_user_del(pokemon_id)
                await User.send(embed=embed)
            else:
                await User.send("Leider ist ein Fehler aufgetreten")


    except:
        print("Fehler in on_message")

@tasks.loop(minutes=1)
async def report_Raids():
    try:
        print("melde Raids")
        GUSTAV.get_all_raids()
        for a in GUSTAV.zu_melden:
            Raid = a
            #print("Melde: ",str(Raid))
            gym_id = Raid[0]
            level = Raid[1]
            GYM = HARRY.give_gym(gym_id)
            gym_details = GYM[0]
            gym = GYM[1]
            raidembed = embed_generator.generate_Raid(Raid, gym, gym_details)

            if level == 1:
                channel_id = config.Channel_LVL_1
            if level == 3:
                channel_id = config.Channel_LVL_3
            if level == 5:
                channel_id = config.Channel_LVL_5
            if level == 6:
                channel_id = config.Channel_LVL_6
            if len(raidembed) >=1:
                embed = raidembed[0]
                user = await bot.fetch_channel(channel_id)
                #user = await bot.fetch_user(308663359316688896)
                await user.send(embed=embed)
            GUSTAV.Raid_gemeldet(a)
    except:
        print("Fehler in Report")

@tasks.loop(minutes=1)
async def report_Pokemon():
    print("melde Pokemon")
    AUGUST.get_all_pokemon()
    for a in AUGUST.zu_melden:
        if config.report_Pokemon == True:
            #print(a)
            #100er
            encounter_id = a[0]
            pokemon_id= a[1]
            latitude= a[2]
            longitude= a[3]
            disappear_time= a[4]
            individual_attack= a[5]
            individual_defense= a[6]
            individual_stamina= a[7]
            cp= a[8]
            pokemonembed = embed_generator.generate_Pokemon(a)
            embed = pokemonembed[0]
            #user = await bot.fetch_user(308663359316688896)
            #await user.send(embed=embed)

            #hunderter
            if individual_attack == 15 and individual_defense == 15 and individual_stamina == 15:
                print("100er gefunden")
                user = await bot.fetch_channel(config.hundert_Channel)
                await user.send(embed=embed)
            if individual_attack != None and individual_defense != None and individual_stamina != None:
                if individual_attack+individual_defense+individual_stamina >= 43:
                    user = await bot.fetch_channel(config.gutChannel)
                    await user.send(embed=embed)
            if pokemon_id in config.selten:
                user = await bot.fetch_channel(config.selten_Channel)
                await user.send(embed=embed)

            #AUGUST.Raid_gemeldet(a) 

        #report Pokemon to user
        if config.report_Pokemon_to_user ==True:
            encounter_id = a[0]
            pokemon_id= a[1]
            latitude= a[2]
            longitude= a[3]
            disappear_time= a[4]
            individual_attack= a[5]
            individual_defense= a[6]
            individual_stamina= a[7]
            cp= a[8]
            pokemonembed = embed_generator.generate_Pokemon(a)
            embed = pokemonembed[0]

            all_user_ohne_iv = GLURAK.is_poke_in_non_iv(pokemon_id)

            for a in all_user_ohne_iv:
                user = await bot.fetch_user(int(a))
                await user.send(embed=embed)

            if individual_attack != None and individual_defense != None and individual_stamina != None:
                if individual_attack+individual_defense+individual_stamina >= 43:
                    all_user_with_iv = GLURAK.is_poke_in_with_iv(pokemon_id)
                    #print(str(pokemon_id))
                    for a in all_user_with_iv:
                        user = await bot.fetch_user(int(a))
                        await user.send(embed=embed)


        AUGUST.Raid_gemeldet(a) 
bot.run(config.Bot_Token)