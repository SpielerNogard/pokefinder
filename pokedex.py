import pickle

def find_pokemon_for_id(pokemon_id, laguage_id):
    with open('jsons/pokedex.data', 'rb') as filehandle:
        Pokemon = []
        # read the data as binary data stream
        allePokemon = pickle.load(filehandle)
        for a in allePokemon:
            pokemon_species_id = a[0]
            local_language_id= a[1]
            name= a[2]
            genus= a[3]
            types= a[4]
            bild= a[5]
            #print(name)
            if int(pokemon_species_id) == pokemon_id and laguage_id == int(local_language_id):
                Pokemon = a
                #print(name)

        return(Pokemon)

def find_pokemon_for_name(pokemon_name,language_id):
    with open('jsons/pokedex.data', 'rb') as filehandle:
        Pokemon = []
        pokemon_name = pokemon_name.lower()
        #print(pokemon_name)
        # read the data as binary data stream
        allePokemon = pickle.load(filehandle)
        for a in allePokemon:
            pokemon_species_id = a[0]
            local_language_id= a[1]
            name= a[2]
            name = name.lower()
            genus= a[3]
            types= a[4]
            bild= a[5]
            #print(name)
            if name == pokemon_name and language_id == int(local_language_id):
                Pokemon = a
                #print(name)
        return(Pokemon)

def give_data_for_everything(eingabe,language_id):
    if "1" in eingabe or "2" in eingabe or "3" in eingabe or "4" in eingabe or "5" in eingabe or "6" in eingabe or "7" in eingabe or "8" in eingabe or "9" in eingabe or "0" in eingabe: 
        Glurak = find_pokemon_for_id(int(eingabe),int(language_id))
        #print(Glurak)
    else:
        Glurak = find_pokemon_for_name(eingabe,int(language_id))
        #print(Glurak)

    return(Glurak)
    
#Glurak = find_pokemon(6, 6)
#print(Glurak)
#give_data_for_everything("Glurak",6)
#give_data_for_everything("6","6")