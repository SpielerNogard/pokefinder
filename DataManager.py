import sched, time
import pickle
from Database_Watcher import Database_Watcher

s = sched.scheduler(time.time, time.sleep)
GUSTAV = Database_Watcher()


def all_raids():
    SQL = "Select * from raid"
    Ergebnis = GUSTAV.read_data(SQL)
    with open('jsons/raids.data', 'wb') as filehandle:
        pickle.dump(Ergebnis, filehandle)

def all_pokestops():
    SQL = "Select * from pokestop"
    Ergebnis = GUSTAV.read_data(SQL)
    with open('jsons/pokestops.data', 'wb') as filehandle:
        pickle.dump(Ergebnis, filehandle)

def all_pokemon():
    SQL = "Select * from pokemon where DATE_ADD(disappear_time, INTERVAL 2 HOUR) > NOW()"
    Ergebnis = GUSTAV.read_data(SQL)
    with open('jsons/pokemon.data', 'wb') as filehandle:
        pickle.dump(Ergebnis, filehandle)

def all_quests():
    SQL = "Select * from trs_quest"
    Ergebnis = GUSTAV.read_data(SQL)
    with open('jsons/quests.data', 'wb') as filehandle:
        pickle.dump(Ergebnis, filehandle)

def all_gyms():
    SQL = "Select * from gym"
    Ergebnis = GUSTAV.read_data(SQL)
    with open('jsons/gym.data', 'wb') as filehandle:
        pickle.dump(Ergebnis, filehandle)

def all_gym_details():
    SQL = "Select * from gymdetails"
    Ergebnis = GUSTAV.read_data(SQL)
    #print(Ergebnis)
    with open('jsons/gym_details.data', 'wb') as filehandle:
        pickle.dump(Ergebnis, filehandle)


def get_all_data(sc):
    all_raids()
    all_gym_details()
    all_gyms()
    all_pokestops()
    all_quests()
    all_pokemon()
    s.enter(30, 1, get_all_data, (sc,))

def get_all_initial_data():
    all_raids()
    all_gym_details()
    all_gyms()
    all_pokestops()
    all_quests()
    all_pokemon()

    
get_all_initial_data()
s.enter(30, 1, get_all_data, (s,))
s.run()
#get_all_data()