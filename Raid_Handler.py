import pickle

class RaidHandler():
    def __init__(self):
        self.gemeldete_Raids = []
        self.zu_melden = []

    def Raid_gemeldet(self,raid):
        self.gemeldete_Raids.append(raid)
        self.zu_melden.remove(raid)

    def get_all_raids(self):
        with open('jsons/raids.data', 'rb') as filehandle:
        # read the data as binary data stream
            alleRaids = pickle.load(filehandle)
            for a in alleRaids:
                gym_id = a[0]
                level= a[1]
                spawn= a[2]
                start = a[3]
                end = a[4]
                pokemon_id = a[5]
                cp = a[6]
                move1 = a[7]
                move2 = a[8]
                last_scanned = a[9]
                form= a[10] 
                is_exclusive = a[11]
                gender= a[12]
                costume = a[13]
                evolution= a[14]
                RAID = [gym_id,level,spawn,start,end,pokemon_id,cp,move1,move2,form,is_exclusive,gender,costume,evolution]
                if RAID not in self.gemeldete_Raids:
                    if RAID not in self.zu_melden:
                        self.zu_melden.append(RAID)
