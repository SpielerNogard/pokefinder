import pickle

class Gym_Handler():
    def __init__(self):
        with open('jsons/gym.data', 'rb') as filehandle:
        # read the data as binary data stream
            allegyms = pickle.load(filehandle)
            #print(allegyms)
        self.gyms = allegyms

        with open('jsons/gym_details.data', 'rb') as filehandle2:
        # read the data as binary data stream
            allegym_details = pickle.load(filehandle2)
        self.gym_details = allegym_details

    def give_gym(self,gym_id_input):
        GYM = []
        for thisgym in self.gym_details:
            #print(gym_details)
            #gym_details
            gym_id = thisgym[0]
            name = thisgym[1]
            description = thisgym[2]
            url = thisgym[3]
            last_scanned= thisgym[4]
            if gym_id_input == gym_id:
                GYM.append(thisgym)
                #print(thisgym)
                #print(name)

        for gym in self.gyms:
            #gym
            gym_id = gym[0]
            team_id = gym[1]
            guard_pokemon_id = gym[2]
            slots_avaible = gym[3]
            enabled = gym[4]
            latitude = gym[5]
            longitude = gym[6]
            total_cp = gym[7]
            is_in_battle = gym[8]
            gender = gym[9]
            form = gym[10]
            costume = gym[11]
            weather_boosted_condition = gym[12]
            shiny = gym[13]
            last_modified = gym[14]
            last_scanned = gym[15]
            is_ex_raid_eligible = gym[16]
            is_ar_scan_eligible = gym[17]
            if gym_id_input == gym_id:
                GYM.append(gym)
                #print(gym)

        return(GYM)

    
#HARRY = Gym_Handler()
#HARRY.give_gym("001dcd349d4849919d16e77ba120cf83.16")