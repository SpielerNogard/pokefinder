import pickle

class User_IV_Handler():
    def __init__(self):
        self.user_with_iv = []
        self.user_without_iv = []
        self.check_files()

    def check_files(self):
        try:
            f = open("jsons/poke_with_iv.data")
            f.close()
            # Do something with the file
        except IOError:
            print("File (jsons/poke_with_iv.data) not accessible, creating it")
            with open('jsons/poke_with_iv.data', 'wb') as filehandle:
                pickle.dump(self.user_with_iv, filehandle)

        try:
            f = open("jsons/poke_without_iv.data")
            f.close()
            # Do something with the file
        except IOError:
            print("File (jsons/poke_without_iv.data) not accessible, creating it")
            with open('jsons/poke_without_iv.data', 'wb') as filehandle:
                pickle.dump(self.user_without_iv, filehandle)
        


    def create_pokemon_without_iv(self,user_id,poki_id):
        try:

            with open('jsons/poke_without_iv.data', 'rb') as filehandle:
            # read the data as binary data stream
                self.user_without_iv =pickle.load(filehandle)

            new_user = [int(user_id),int(poki_id)]
            self.user_without_iv.append(new_user)

            with open('jsons/poke_without_iv.data', 'wb') as filehandle:
                    pickle.dump(self.user_without_iv, filehandle)

            return(True)
        except:
            return(False)
    
    def create_pokemon_with_iv(self,user_id,poki_id):
        try:

            with open('jsons/poke_with_iv.data', 'rb') as filehandle:
            # read the data as binary data stream
                self.user_with_iv =pickle.load(filehandle)

            new_user = [int(user_id),int(poki_id)]
            self.user_with_iv.append(new_user)

            with open('jsons/poke_with_iv.data', 'wb') as filehandle:
                    pickle.dump(self.user_with_iv, filehandle)

            return(True)
        except:
            return(False)

    def find_my_list(self,user_id):
        try:
            Ergebnis = []
            mit = []
            ohne = []
            with open('jsons/poke_with_iv.data', 'rb') as filehandle:
            # read the data as binary data stream
                self.user_with_iv =pickle.load(filehandle)

            with open('jsons/poke_without_iv.data', 'rb') as filehandle:
            # read the data as binary data stream
                self.user_without_iv =pickle.load(filehandle)
            for a in self.user_with_iv:
                user_id_in_dok = a[0]
                Pokemon_id = a[1]
                if int(user_id_in_dok) == int(user_id):
                    mit.append(Pokemon_id)

            for b in self.user_without_iv:
                user_id_in_dok = b[0]
                Pokemon_id = b[1]
                if int(user_id_in_dok) == int(user_id):
                    ohne.append(Pokemon_id)

            Ergebnis = [ohne,mit]
            return(Ergebnis)

        except:
            Ergebnis = ["Ich konnte keine Pokemon-Liste finden","Ich konnte keine IV-Liste finden"]
            return(Ergebnis)

    def delete_pokemon_from_list(self,user_id,pokemon_id):
        try:
            
            Suche = [int(user_id),int(pokemon_id)]
            #print(Suche)
            with open('jsons/poke_with_iv.data', 'rb') as filehandle:
            # read the data as binary data stream
                self.user_with_iv =pickle.load(filehandle)

            with open('jsons/poke_without_iv.data', 'rb') as filehandle:
            # read the data as binary data stream
                self.user_without_iv =pickle.load(filehandle)

            print(str(self.user_with_iv))
            if Suche in self.user_with_iv:
                self.user_with_iv.remove(Suche)
            if Suche in self.user_without_iv:
                self.user_without_iv.remove(Suche)
            with open('jsons/poke_with_iv.data', 'wb') as filehandle:
                    pickle.dump(self.user_with_iv, filehandle)

            with open('jsons/poke_without_iv.data', 'wb') as filehandle:
                    pickle.dump(self.user_without_iv, filehandle)

            return(True)
        except:
            return(False)

    def is_user_in_non_iv(self,user_id, pokemon_id):
        Ergebnis = False
        with open('jsons/poke_without_iv.data', 'rb') as filehandle:
            # read the data as binary data stream
                self.user_without_iv =pickle.load(filehandle)
        
        new_user = [int(user_id),int(pokemon_id)]
        if new_user in self.user_without_iv:
            Ergebnis = True

        return(Ergebnis)

    def is_user_in_with_iv(self,user_id, pokemon_id):
        Ergebnis = False
        with open('jsons/poke_with_iv.data', 'rb') as filehandle:
            # read the data as binary data stream
                self.user_with_iv =pickle.load(filehandle)
        
        new_user = [int(user_id),int(pokemon_id)]
        if new_user in self.user_with_iv:
            Ergebnis = True

        return(Ergebnis)





#baum = User_IV_Handler()