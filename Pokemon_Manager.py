import pickle

class Pokemon_Manager():
    def __init__(self):
        self.gemeldete_Pokemon = []
        self.zu_melden = []

    def Raid_gemeldet(self,pokemon):
        self.gemeldete_Pokemon.append(pokemon)
        if pokemon in self.zu_melden:
            self.zu_melden.remove(pokemon)

    def get_all_pokemon(self):
        with open('jsons/pokemon.data', 'rb') as filehandle:
        # read the data as binary data stream
            allepokemon = pickle.load(filehandle)
            for a in allepokemon:
                encounter_id = a[0]
                spawnpoint_id= a[1]
                pokemon_id= a[2]
                latitude = a[3]
                longitude = a[4]
                disappear_time = a[5]
                individual_attack = a[6]
                individual_defense = a[7]
                individual_stamina = a[8]
                move_1 = a[9]
                move_2= a[10] 
                cp = a[11]
                cp_multiplier= a[12]
                weight = a[13]
                height= a[14]
                gender = a[15]
                form= a[16] 
                costume = a[17]
                catch_prob_1 = a[18]
                catch_prob_2= a[19]
                catch_prob_3 = a[20]
                rating_attack = a[21]
                rating_defense = a[22]
                weather_boosted_condition= a[23]
                last_modified= a[24]
                POKEMON = [encounter_id,pokemon_id,latitude,longitude,disappear_time,individual_attack,individual_defense,individual_stamina,cp]
                if POKEMON not in self.gemeldete_Pokemon:
                    if POKEMON not in self.zu_melden:
                        self.zu_melden.append(POKEMON)
